{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit pkSalesSwitch;

{$warn 5023 off : no warning about unused units}
interface

uses
  untsalesSwitch, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('untsalesSwitch', @untsalesSwitch.Register);
end;

initialization
  RegisterPackage('pkSalesSwitch', @Register);
end.
