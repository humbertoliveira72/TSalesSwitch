unit untsalesSwitch;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,ExtCtrls, Graphics,LResources,
  LMessages;

type

  { TSalesSwitch }
  TSize = (scSmall,scMedium,scGreat,scBig);
  TSkin = (ctSwitch,ctSwitch1,ctSwitch2,ctSwitch3,ctSwitch4,ctSwitch5,ctSwitch6,ctSwitch7,ctSwitch8,ctSwitch9,ctSwitch10,ctSwitch11,ctSwitch12,ctSwitch13,ctSwitch14,ctSwitch15,ctSwitch16,ctSwitch17);

  TSalesSwitch = class(TCustomImage)
  private
    FChecked: boolean;
    FOnChecked: TNotifyEvent;
    FSize: TSize;
    FSkin: TSkin;
    procedure SalesCheckClick(Sender: TObject);
    procedure doPicture;
    procedure SetChecked(AValue: boolean);
    procedure SetSize(AValue: TSize);
    procedure SetSkin(AValue: TSkin);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Size       : TSize        read FSize       write SetSize;
    property Skin       : TSkin        read FSkin       write SetSkin;
    property Checked    : boolean      read FChecked    write SetChecked default false;
    property OnChecked  : TNotifyEvent read FOnChecked  write FOnChecked;
    property Anchors;
    property BorderSpacing;
    property ShowHint;
    property Visible;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    property OnMouseWheelHorz;
    property OnMouseWheelLeft;
    property OnMouseWheelRight;
    property OnPaint;
    property OnPictureChanged;
    property OnPaintBackground;
  end;

  procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Sales',[TSalesSwitch]);
end;

{ TSalesSwitch }


procedure TSalesSwitch.doPicture;
begin
  case FSkin of
    ctSwitch   :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checked-Small');
                false: picture.LoadFromLazarusResource('unchecked-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checked-Medium');
                false: picture.LoadFromLazarusResource('unchecked-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checked-Great');
                false: picture.LoadFromLazarusResource('unchecked-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checked-Big');
                false: picture.LoadFromLazarusResource('unchecked-Big');
                end;
         end;
    ctSwitch1  :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedA-Small');
                false: picture.LoadFromLazarusResource('uncheckedA-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedA-Medium');
                false: picture.LoadFromLazarusResource('uncheckedA-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedA-Great');
                false: picture.LoadFromLazarusResource('uncheckedA-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedA-Big');
                false: picture.LoadFromLazarusResource('uncheckedA-Big');
                end;
         end;
    ctSwitch2  :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedB-Small');
                false: picture.LoadFromLazarusResource('uncheckedB-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedB-Medium');
                false: picture.LoadFromLazarusResource('uncheckedB-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedB-Great');
                false: picture.LoadFromLazarusResource('uncheckedB-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedB-Big');
                false: picture.LoadFromLazarusResource('uncheckedB-Big');
                end;
         end;
    ctSwitch3  :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedC-Small');
                false: picture.LoadFromLazarusResource('uncheckedC-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedC-Medium');
                false: picture.LoadFromLazarusResource('uncheckedC-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedC-Great');
                false: picture.LoadFromLazarusResource('uncheckedC-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedC-Big');
                false: picture.LoadFromLazarusResource('uncheckedC-Big');
                end;
         end;
    ctSwitch4  :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedD-Small');
                false: picture.LoadFromLazarusResource('uncheckedD-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedD-Medium');
                false: picture.LoadFromLazarusResource('uncheckedD-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedD-Great');
                false: picture.LoadFromLazarusResource('uncheckedD-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedD-Big');
                false: picture.LoadFromLazarusResource('uncheckedD-Big');
                end;
         end;
    ctSwitch5  :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedE-Small');
                false: picture.LoadFromLazarusResource('uncheckedE-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedE-Medium');
                false: picture.LoadFromLazarusResource('uncheckedE-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedE-Great');
                false: picture.LoadFromLazarusResource('uncheckedE-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedE-Big');
                false: picture.LoadFromLazarusResource('uncheckedE-Big');
                end;
         end;
    ctSwitch6  :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedF-Small');
                false: picture.LoadFromLazarusResource('uncheckedF-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedF-Medium');
                false: picture.LoadFromLazarusResource('uncheckedF-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedF-Great');
                false: picture.LoadFromLazarusResource('uncheckedF-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedF-Big');
                false: picture.LoadFromLazarusResource('uncheckedF-Big');
                end;
         end;
    ctSwitch7  :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedG-Small');
                false: picture.LoadFromLazarusResource('uncheckedG-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedG-Medium');
                false: picture.LoadFromLazarusResource('uncheckedG-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedG-Great');
                false: picture.LoadFromLazarusResource('uncheckedG-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedG-Big');
                false: picture.LoadFromLazarusResource('uncheckedG-Big');
                end;
         end;
    ctSwitch8  :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedH-Small');
                false: picture.LoadFromLazarusResource('uncheckedH-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedH-Medium');
                false: picture.LoadFromLazarusResource('uncheckedH-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedH-Great');
                false: picture.LoadFromLazarusResource('uncheckedH-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedH-Big');
                false: picture.LoadFromLazarusResource('uncheckedH-Big');
                end;
         end;
    ctSwitch9  :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedI-Small');
                false: picture.LoadFromLazarusResource('uncheckedI-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedI-Medium');
                false: picture.LoadFromLazarusResource('uncheckedI-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedI-Great');
                false: picture.LoadFromLazarusResource('uncheckedI-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedI-Big');
                false: picture.LoadFromLazarusResource('uncheckedI-Big');
                end;
         end;
    ctSwitch10 :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedJ-Small');
                false: picture.LoadFromLazarusResource('uncheckedJ-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedJ-Medium');
                false: picture.LoadFromLazarusResource('uncheckedJ-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedJ-Great');
                false: picture.LoadFromLazarusResource('uncheckedJ-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedJ-Big');
                false: picture.LoadFromLazarusResource('uncheckedJ-Big');
                end;
         end;
    ctSwitch11 :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedK-Small');
                false: picture.LoadFromLazarusResource('uncheckedK-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedK-Medium');
                false: picture.LoadFromLazarusResource('uncheckedK-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedK-Great');
                false: picture.LoadFromLazarusResource('uncheckedK-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedK-Big');
                false: picture.LoadFromLazarusResource('uncheckedK-Big');
                end;
         end;
    ctSwitch12 :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedL-Small');
                false: picture.LoadFromLazarusResource('uncheckedL-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedL-Medium');
                false: picture.LoadFromLazarusResource('uncheckedL-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedL-Great');
                false: picture.LoadFromLazarusResource('uncheckedL-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedL-Big');
                false: picture.LoadFromLazarusResource('uncheckedL-Big');
                end;
         end;
    ctSwitch13 :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedM-Small');
                false: picture.LoadFromLazarusResource('uncheckedM-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedM-Medium');
                false: picture.LoadFromLazarusResource('uncheckedM-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedM-Great');
                false: picture.LoadFromLazarusResource('uncheckedM-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedM-Big');
                false: picture.LoadFromLazarusResource('uncheckedM-Big');
                end;
         end;
    ctSwitch14 :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedN-Small');
                false: picture.LoadFromLazarusResource('uncheckedN-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedN-Medium');
                false: picture.LoadFromLazarusResource('uncheckedN-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedN-Great');
                false: picture.LoadFromLazarusResource('uncheckedN-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedN-Big');
                false: picture.LoadFromLazarusResource('uncheckedN-Big');
                end;
         end;

    ctSwitch15 :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedO-Small');
                false: picture.LoadFromLazarusResource('uncheckedO-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedO-Medium');
                false: picture.LoadFromLazarusResource('uncheckedO-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedO-Great');
                false: picture.LoadFromLazarusResource('uncheckedO-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedO-Big');
                false: picture.LoadFromLazarusResource('uncheckedO-Big');
                end;
         end;
    ctSwitch16 :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedP-Small');
                false: picture.LoadFromLazarusResource('uncheckedP-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedP-Medium');
                false: picture.LoadFromLazarusResource('uncheckedP-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedP-Great');
                false: picture.LoadFromLazarusResource('uncheckedP-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedP-Big');
                false: picture.LoadFromLazarusResource('uncheckedP-Big');
                end;
         end;
    ctSwitch17 :
         case FSize of
           scSmall  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedQ-Small');
                false: picture.LoadFromLazarusResource('uncheckedQ-Small');
                end;
           scMedium :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedQ-Medium');
                false: picture.LoadFromLazarusResource('uncheckedQ-Medium');
                end;
           scGreat  :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedQ-Great');
                false: picture.LoadFromLazarusResource('uncheckedQ-Great');
                end;
           scBig    :
                Case FChecked of
                true : picture.LoadFromLazarusResource('checkedQ-Big');
                false: picture.LoadFromLazarusResource('uncheckedQ-Big');
                end;
         end;
  end;

end;

procedure TSalesSwitch.SetChecked(AValue: boolean);
begin
  if FChecked=AValue then Exit;
  FChecked:=AValue;
  if FOnChecked <> nil then
    FOnChecked(Self);
  doPicture;

end;

procedure TSalesSwitch.SetSize(AValue: TSize);
begin
  if FSize=AValue then Exit;
  FSize:=AValue;
  doPicture;
end;

procedure TSalesSwitch.SetSkin(AValue: TSkin);
begin
  if FSkin=AValue then Exit;
  FSkin:=AValue;
  doPicture;
end;

procedure TSalesSwitch.SalesCheckClick(Sender: TObject);
begin
  Checked := not Checked
end;

constructor TSalesSwitch.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  picture.LoadFromLazarusResource('unchecked-Small');
  AutoSize := true;
  onclick  := @SalesCheckClick;
end;

destructor TSalesSwitch.Destroy;
begin
  inherited Destroy;
end;

initialization
{$i SalesSwitch.lrs}

end.

